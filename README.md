# README

[![PyPI Latest Version](https://badge.fury.io/py/tpg-256a-pressure-monitor.svg)](https://badge.fury.io/py/tpg-256a-pressure-monitor)
[![pipeline status](https://gitlab.ethz.ch/exotic-matter/cw-beam/tpg-256a-pressure-monitor/badges/master/pipeline.svg)](https://gitlab.ethz.ch/exotic-matter/cw-beam/tpg-256a-pressure-monitor/-/commits/master)
[![coverage report](https://gitlab.ethz.ch/exotic-matter/cw-beam/tpg-256a-pressure-monitor/badges/master/coverage.svg)](https://gitlab.ethz.ch/exotic-matter/cw-beam/tpg-256a-pressure-monitor/-/commits/master)
[![Documentation Status](https://readthedocs.org/projects/tpg-256a-pressure-monitor/badge/?version=stable)](https://tpg-256a-pressure-monitor.readthedocs.io/en/stable/?badge=table)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

## TPG - 256A Pressure Monitor

The **tpg-256a-pressure-monitor** is a readout and logging software for the Pfeiffer TPG-256A Pressure Controller. The
package includes a device driver which manages the connection and communication through RS-232 serial port. The
application is run as a command-line 

## Dependencies

The package **tpg-256a-pressure-monitor** has the following pre-requisites:

 -  [Python 3.6](https://www.python.org/downloads/release/python-360/) and [pip 10.0](https://pip.pypa.io/en/stable/)
    are the minimum required versions to build and install **tpg-256a-pressure-monitor** and its dependencies. It is
    recommended to install and run **tpg-256a-pressure-monitor** (and any other package, for that matter) under a
    [virtual environment](https://docs.python.org/3/library/venv.html).

 -  [libpq](https://www.postgresql.org/docs/11/libpq.html), a C library that implements connections to the PostgreSQL
    backend server. It is used by the package [lab-utils](https://gitlab.ethz.ch/exotic-matter/cw-beam/lab-utils) to
    connect to the PostgreSQL backend, manage database structure and save new data entries.

## Getting Started

TODO: pip package available now 

## Usage

To use a the **tpg-256a-pressure-monitor** package, TODO



## Authors

* [**Carlos Vigo**](mailto:carlosv@phys.ethz.ch?subject=[GitHub%-%lab-utils]) - *Initial work* - 
[GitLab](https://gitlab.ethz.ch/carlosv)

## Contributing

Please read our [contributing policy](CONTRIBUTING.md) for details on our code of
conduct, and the process for submitting pull requests to us.

## Versioning

We use [Git](https://git-scm.com/) for versioning. For the versions available, see the 
[tags on this repository](https://gitlab.ethz.ch/exotic-matter/cw-beam/tpg-256a-pressure-monitor).

## License

This project is licensed under the [GNU GPLv3 License](LICENSE.md)

## Built With

* [PyCharm Community Edition](https://www.jetbrains.com/pycharm//) - The IDE used
* [Sphinx](https://www.sphinx-doc.org/en/master/index.html) - Documentation

## Acknowledgments

* Nobody so far
