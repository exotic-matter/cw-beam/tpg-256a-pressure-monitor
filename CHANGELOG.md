# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.0] - 2020-06-19

- Implement autostart option to launch with supervisord

## [0.2.0] - 2020-05-26

- Full implementation of the logging module

## [0.1.0] - 2020-04-21

- First release of the **tpg-256a-pressure-monitor** package
- Installation instructions and setup

[0.1.0]: https://gitlab.ethz.ch/exotic-matter/cw-beam/tpg-256a-pressure-monitor/tree/v0.1.0
[0.2.0]: https://gitlab.ethz.ch/exotic-matter/cw-beam/tpg-256a-pressure-monitor/tree/v0.2.0
[0.3.0]: https://gitlab.ethz.ch/exotic-matter/cw-beam/tpg-256a-pressure-monitor/tree/v0.3.0