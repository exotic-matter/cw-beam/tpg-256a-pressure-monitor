Welcome to the TPG-256A Pressure Monitor Documentation!
=======================================================

.. image:: https://badge.fury.io/py/tpg-256a-pressure-monitor.svg
    :target: https://badge.fury.io/py/tpg-256a-pressure-monitor
    :alt: PyPI Latest Version

.. image:: https://gitlab.ethz.ch/exotic-matter/cw-beam/tpg-256a-pressure-monitor/badges/master/pipeline.svg
    :target: https://gitlab.ethz.ch/exotic-matter/cw-beam/tpg-256a-pressure-monitor/-/commits/master
    :alt: Pipeline Status

.. image:: https://gitlab.ethz.ch/exotic-matter/cw-beam/tpg-256a-pressure-monitor/badges/master/coverage.svg
    :target: https://gitlab.ethz.ch/exotic-matter/cw-beam/tpg-256a-pressure-monitor/-/commits/master
    :alt: Coverage Report

.. image:: https://readthedocs.org/projects/tpg-256a-pressure-monitor/badge/?version=latest
    :target: https://tpg-256a-pressure-monitor.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

.. image:: https://img.shields.io/badge/License-GPLv3-blue.svg
    :target: https://www.gnu.org/licenses/gpl-3.0
    :alt: License: GPL v3

To be completed...

You can also check the :download:`official documentation<Pfeiffer_MultiGauge256A_OpInstructions.pdf>`



.. toctree::
    :maxdepth: 1

    Welcome <self>
    Readme <project/README.md>
    api
    Changelog <project/CHANGELOG.md>
    Contributing <project/CONTRIBUTING.md>
    License <project/LICENSE.md>
