# -*- coding: utf-8 -*-
# Author: Carlos Vigo
# Contact: carlosv@phys.ethz.ch

""" Readout and logging application for the Pfeiffer
TPG-256A Pressure Readout.
"""

# Local imports
from . import __project__, tpg_256a_pressure_monitor

__all__ = [
    __project__.__author__,
    __project__.__copyright__,
    __project__.__short_version__,
    __project__.__version__,
    __project__.__project_name__,
    'tpg_256a_pressure_monitor'
]
